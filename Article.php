

<?php

	class Article
	{



		static function pullLatest($howMany = null) // null by default unless specified when calling the function.
		{

			global $conn;

			$query = 'SELECT * FROM articles ORDER BY post_date';
			$conn_status = mysqli_query($conn, $query);

			$slider_info = [];

			while($row = $conn_status->fetch_assoc())
			{
				// $row is an article. If u return $row, then JS will loop only 1 article.
				// You must put each article ($row)in an array and return the array
				// Then JS will loop all articles
				array_push($slider_info, $row);
			}
			return $slider_info;
		}




		static function add($article_title, $article_text, $upload_photo)
		{

			// check if it's image file
			$imageFileType = strtolower(pathinfo($upload_photo["name"],PATHINFO_EXTENSION));

			if ($imageFileType != "jpg" && $imageFileType != "png" && $imageFileType != "jpeg" && $imageFileType != "gif" )
			{
			    return "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
			}

			// check file size
			if ($upload_photo["size"] > 500000)
			{
			    return "Sorry, your file is too large";
			}

			// check if file already exists
			if (file_exists("uploads/" . $upload_photo["name"]))
			{
			    return "Sorry, file already exists";
			}

			// upload photo
			$upload_status = move_uploaded_file($upload_photo['tmp_name'], "uploads/" . $upload_photo['name']);				

			if (!$upload_status)
			{
				return 'There was a problem uploading the file, please try again.';
			}

			//write to sql
			$article_url = $article_title;
			$char_replace_from = array(' ', ',', '?', ':', '\'', '-', 'ı', 'ş', 'ö', 'ç', 'ğ', 'ü', 'İ', 'Ğ', 'Ö', 'Ç', 'Ş', 'Ü');
			$char_replace_to   = array('_', '' , '' , '' , ''  , '' , 'i', 's', 'o', 'c', 'g', 'u', 'I', 'G', 'O', 'C', 'S', 'U');
			$article_url = str_replace($char_replace_from, $char_replace_to, $article_url);
			$article_url = mb_strtolower($article_url);
		
			global $conn;

			$article_title =  $conn->real_escape_string($article_title);
			$article_text =  $conn->real_escape_string($article_text);
			$photo = $conn->real_escape_string($upload_photo['name']);
			$article_url =  $conn->real_escape_string($article_url);

			$query = 'INSERT INTO articles (title, text, photo, url) VALUES ("'.$article_title.'", "'.$article_text.'", "'.$photo.'", "'.$article_url.'")';

			$conn_status = mysqli_query($conn, $query);

			if ($conn_status)
			{
				return 'Success!';
			}
			
			return 'Failed inserting data into the database.';
		}




		static function pullOne($article_name)//name or id, whatever ur system needs
		{
			//when going to article page directly  u must pull specific article.
		}



	}

?>