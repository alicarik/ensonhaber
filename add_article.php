<?php

	ini_set('display_errors', 1);
	ini_set('display_startup_errors', 1);
	error_reporting(E_ALL);

	session_start();

	if (!isset($_SESSION['username']))
	{
		header('Location: login.php');
	}

	else
	{
		echo '<div style="float:right;">welcome, ' . $_SESSION['username'] . '<br>
				<form method="POST">
					<input type="submit" name="logout" value="logout" />
				</form>
			</div>';
	}

	if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST['back_to_menu']))
	{
		header('Location: admin.php');
	}

	if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST['logout']))
	{
		session_destroy();
		header('Location: login.php');
		exit();
	}

?>



<form method="POST">
	<input type="submit" name="back_to_menu" value="Back to menu" />
</form>


<br>

<form enctype="multipart/form-data">
	article title
	<textarea id="article_title" style="resize:none;width:400px;height:30px;"></textarea>
	<br>
	<br>
	article text
	<textarea id="article_text" style="resize:none;width:400px;height:400px;font-size:20px;"></textarea>
	<br>
	<br>
	<input type="file" id="upload_photo" />
	<br>
	<button id="submit_new_article">Submit New Article</button>
</form>


<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<script type="text/javascript">

	$(document).ready(function(){
		$("#submit_new_article").on('click', function(e) {

			e.preventDefault();
			var formData = new FormData(),
				article_title = $('textarea#article_title').val(),
				article_text  = $('textarea#article_text').val(),
				// to pull files from html input, browser needs "real" JavaScript element.
				upload_photo = $('#upload_photo')[0].files[0]; // [0] selects "real" native html element.
			formData.append('article_title', article_title);
			formData.append('article_text' , article_text);
			formData.append('upload_photo' , upload_photo);
			formData.append('action', 'article-add');

   			$.ajax({
			    type: "POST",
			    url: "routes.php",
			    data: formData,
			    processData: false, // needed when using FormData
			    contentType: false, // needed when using FormData
			    dataType: "json",
			    success: function (data)
			    {
			    	alert(data);
			 	}
			});

        });
    });

</script>