<!DOCTYPE html>
<html>
<head>
	<title>News Portal</title>
	<link rel="stylesheet" type="text/css" href="style.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>


	<style>
	table, th, td
	{
	    border: 1px solid black;
	    border-collapse: collapse;
	}
	th, td
	{
	    padding: 15px;
	}
	</style>
</head>
<body>

<div id="someElement">
	{{changedComment}}
</div>


<?php

	session_start();

	if (!isset($_SESSION['username']))
	{
		header('Location: login.php');
	}

	else
	{
		echo '<div style="float:right;">welcome, ' . $_SESSION['username'] . '<br>
				<form method="POST">
					<input type="submit" name="logout" value="logout" />
				</form>
			</div>';
	}

	if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST['logout']))
	{
		echo "you want to logout!";
		session_destroy();
		header('Location: login.php');
	}

	echo "<br><br><br>";

	include 'connection.php';


	error_reporting(E_ALL);
	ini_set('display_errors', 1);

	$query_moderation = 'SELECT c.id AS comment_id, c.comment_name, c.comment_text, a.title
						FROM comments AS c
						JOIN articles AS a
						ON c.article_id=a.id
						WHERE c.approved IS NULL';

	$conn_status_moderation = mysqli_query($conn, $query_moderation);

	echo '<table>
				<tr style="font-weight:bold;">
					<th>Comment Nickname</th>
					<th>Comment Text</th> 
					<th>Article</th>
					<th>Action</th>
				</tr>';

	while($row_moderation = $conn_status_moderation->fetch_assoc())
	{
		echo '<tr id="comment-row-'.$row_moderation['comment_id'].'">
					<td>'
						 . $row_moderation['comment_name'] .
					'</td>
					 <td><i>'
						 . $row_moderation['comment_text'] .
					"</i></td>
					<td>"
						 . $row_moderation['title'] .
					'</td>
					<td style="width:140px;">'
						.
						 	'<div class="comment-decision" data-decision="1" data-commentid="'.$row_moderation['comment_id'].'">Onayla</div>
						    <div class="middle_separator">&nbsp|&nbsp</div>
						    <div class="comment-decision" data-decision="0" data-commentid="'.$row_moderation['comment_id'].'">Reject Et</div>'
						.
					"</td>
			</tr>";
	}

	echo "</table>";


	echo "<br><br><br>";

	echo '<a href="add_article.php" style="font-weight:bold;font-size:28px;">Add a New Article</a>';

	echo "<br>";



	$query = 'SELECT * FROM articles';
	$conn_status = mysqli_query($conn, $query);

	echo '<table>
				<tr style="font-weight:bold;">
					<th>title</th>
					<th>text</th>
					<th>photo</th>
				</tr>';

	while($row = $conn_status->fetch_assoc())
	{
		echo "<tr>
					<td>"
						 . $row['title'] .
					'</td>
					 <td style="width:400px;">' 
						 . $row['text'] .
					"</td>
					<td>"
						 . '<img src="uploads/' . $row['photo'] . '" height="100" width="250">' .
					"</td>
			</tr>";
	}
	echo "</table>";


?>



<script src="https://cdnjs.cloudflare.com/ajax/libs/vue/2.5.13/vue.min.js"></script>

<script type="text/javascript">

	$(document).ready(function(){
		// --- New Material ---
		var app = new Vue({
			el: '#someElement',
			data: {
				changedComment: 'I am a cunt!'
			}
		});

		$(".comment-decision").on('click', function() {
			var decision = $(this).data('decision');
   			var comment_id = $(this).data("commentid");
   			alert("comment id: " + comment_id + " decision: " + decision);
   			$.ajax({
			    type: "POST",
			    url: "routes.php",
			    dataType: "json",
			    data: {
			       	decision: decision,
			     	comment_id: comment_id,
			     	action: 'comment-moderate'
			    },
			    success: function (data) {
					$("#comment-row-" + comment_id).hide();
					app.changedComment = comment_id; //same like $("#id_there").html(comment_id)
			 	}
			});
        });
    });

</script>



</body>
</html>

