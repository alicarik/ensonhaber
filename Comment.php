
<?php


	class Comment
	{
		static function send($commentnick, $commenttext, $article_id)
		{
			global $conn;

			$commentnick =  $conn->real_escape_string($commentnick);
			$commenttext = $conn->real_escape_string($commenttext);
			$article_id = $conn->real_escape_string($article_id);

			$query = 'INSERT INTO comments (comment_name, comment_text, article_id)
					VALUES ("'.$commentnick.'","'.$commenttext.'","'.$article_id.'")';
			$conn_status = mysqli_query($conn, $query);

			return true;
		}


		static function moderate($decision, $comment_id)
		{
			global $conn;

			$decision = $conn->real_escape_string($decision);
			$comment_id =  $conn->real_escape_string($comment_id);

			$query = 'UPDATE comments SET approved="' . $decision . '" WHERE id ="' . $comment_id . '"';
			$conn_status = mysqli_query($conn, $query);

			return true;
		}
	}



?>