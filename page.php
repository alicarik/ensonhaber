
<?php
	include 'connection.php';
	$url_request = $_GET['url'];
	// echo 'ARTICLE: ' . $url_request;
	$query = 'SELECT * FROM articles WHERE url="' . $url_request . '"';
	$conn_status = mysqli_query($conn, $query);
	$row = $conn_status->fetch_assoc();
	$article_id = $row['id'];

?>


<!DOCTYPE html>
<html>


<head>
	<title>News Portal</title>
	<link rel="stylesheet" type="text/css" href="style.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script src="https://unpkg.com/sweetalert@2.1.0/dist/sweetalert.min.js"></script>
</head>


<body>

	<div id="uppercontainer">
 		<a href="index.php">
			<div id="logo"><img src="uploads/logo.jpeg"/></div>
		</a>
		<div id="weatherforecast">weather forecast</div>
		<div id="search">
			<form>
				<input type="text" id="search_text" value="Arama yapiniz..">
				<input type="submit" id="search_submit" value="Submit">
			</form>
		</div>
	</div>


	<div id="page-container">
		<div id="title-box">
			<?php
				echo $row['title'];
			?>
		</div>

		<div id="photo-box">
			<?php
				echo '<img src="uploads/' . $row['photo'] . '">';
			?>
		</div>

		<div id="text-box">
			<?php
				echo $row['text'];
			?>
		</div>
	</div>


	<div id="comment-container1">
		<div id="comment-container2">
			<div id="comment-header"><b>Yorumlar:</b></div>
			<br>
			<div id="comment-container3">Yorumunuz: <textarea rows="4" cols="57" id="commenttext"></textarea></div>
			<div id="comment-container4">
				<div>Adınız</div><input type="text" id="commentnick">
				<div style="margin-top:4px;"><input type="submit" id="commentsend" value="Yorum Gönder"></div>
			</div>
			<br><br><br><br><br><br>
		</div>
		<div id="comment-container5">
			<?php
				//$query_show_comments = 'SELECT * FROM comments WHERE article_id="' . $article_id . '"';
				$query_show_comments = 'SELECT * FROM comments WHERE article_id="' . $article_id . '"' . 'AND approved="1"';
				$conn_status_show_comments = mysqli_query($conn, $query_show_comments);
				while($row_show_comments = $conn_status_show_comments->fetch_assoc())
				{
					echo $row_show_comments["comment_name"] . "<br>";
					echo $row_show_comments["comment_text"] . "<br>";
					echo $row_show_comments["reg_date"]     . "<br><br>";
				}
			?>
		</div>
	</div>


	<script>
	$(document).ready(function () {
	    $('#commentsend').on('click',function () {
	        swal({
	            title: "Emin misiniz?",
	            text: "Yorumunuzu gönderiyoruz",
	            icon: "warning",
	            buttons: {
	                confirm: 'Evet',
	                cancel: 'Hayır'
	            },
	        }).then(function (isConfirm) {
	            if (isConfirm)
	            {
	            	var commentnick = $("#commentnick").val();
			    	var commenttext = $("#commenttext").val();
			    	var article_id = "<?php echo $article_id; ?>";

			       	$.ajax({
					    type: "POST",
					    url: "routes.php",
					    dataType : "json",
						data: {
							   commentnick: commentnick,
							   commenttext: commenttext,
							   article_id : article_id,
							   action: 'comment-send'
							  },
					    success: function (data)
					    {
					    	alert("top");
					    	alert(data);
					    	$("#comment-container3").html("Yorumunuz gönderilmiştir. Moderatör onayından sonra yayınlanacaktır.");
					    	$("#comment-container4").html("");
					 	}
					});
                    swal("Gönderildi!", "Yorumunuz gönderilmiştir!", "success");
	            }
	            else
	            {
	                swal("İptal edildi!", "Yorum gönderimi iptal edilmiştir!", "error");
	            }
	        });
	    });
	});
	</script>


</body>
</html>