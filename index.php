
<?php
	include "slider.php";
?>


<!DOCTYPE html>
<html>


<head>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script src="http://cdn.jsdelivr.net/jquery.slick/1.3.13/slick.min.js"></script>
	<link rel="stylesheet" type="text/css" href="http://cdn.jsdelivr.net/jquery.slick/1.3.13/slick.css">
	<link rel="stylesheet" type="text/css" href="style.css">
</head>


<body>
	<div id="uppercontainer">

		<div id="logo"><a href="index.php"><img src="uploads/logo.jpg"/></a></div>
		
		<div id="weatherforecast">weather forecast</div>
		<div id="search">
			<form>
				<input type="text" id="search_text" value="Arama yapiniz..">
				<input type="submit" id="search_submit" value="Submit">
			</form>
		</div>
	</div>

	<div id="middlecontainer">

		<div id="big_slider">
			<a href="" target="_blank">
			</a>
		</div>

		<div id="number_container">
			<div class="numbers"><a href="" target="_blank">1</a></div>
			<div class="numbers"><a href="" target="_blank">2</a></div>
			<div class="numbers"><a href="" target="_blank">3</a></div>
			<div class="numbers"><a href="" target="_blank">4</a></div>
			<div class="numbers"><a href="" target="_blank">5</a></div>
			<div class="numbers"><a href="" target="_blank">6</a></div>
			<div class="numbers"><a href="" target="_blank">7</a></div>
			<div class="numbers"><a href="" target="_blank">8</a></div>
			<div class="numbers"><a href="" target="_blank">9</a></div>
			<div class="numbers"><a href="" target="_blank">10</a></div>
		</div>

 		<div id="arrow_container">
		    <button id="arrow1" class="btn-prev"></button>
		</div>

		<div id="dashboard">
		</div>

		<div id="arrow_container">
		    <button id="arrow2" class="btn-next"></button>
		</div>

	</div>


	<script>
	    $(document).ready(function () {

	    	var photoArray = [];
	    	var urlArray = [];

            $.ajax({
                method: 'post',
                dataType: 'json',
                data: {
				    	action: 'article-pull'
				      },
                url: 'routes.php', 
                success: function (data)
                {
                	for (var i = 0; i < data.length; i++)
                	{
                        photoArray.push(data[i].photo);
                        urlArray.push(data[i].url);
	                }

                	$("#big_slider").css("background-image", "url(uploads/" + photoArray[0] + ")");
                	$("#big_slider a").attr("href", "page.php?url=" + urlArray[0]);
                	$(".numbers:first-child").css("background-color", "white");
                	$(".numbers:first-child a").attr("href", "page.php?url=" + urlArray[0]);

                	var html = '';
                	for (var i = 10; i < photoArray.length; i++)
                	{
                		html += '<div class="bottom_article" style="background-image: url(uploads/'+photoArray[i]+')"><a href="page.php?url='+urlArray[i]+'" target="_blank"></a></div>';
                	}
                	$('#dashboard').html(html);

				    var slickOpts = {
				    	speed: 800,
				        slidesToShow: 4,
				        slidesToScroll: 4,
				        dots: true,
				        prevArrow: '.btn-prev',
				        nextArrow: '.btn-next'
				    };
				    $('#dashboard').slick(slickOpts);
                }
            });

	        $(".numbers").hover(function(){
	        	$(".numbers").css("background-color", "lightgrey");
	        	var selectedNumber = Number($(this).text()) - 1;
			    $('#big_slider').css("background-image", "url(uploads/" + photoArray[selectedNumber]  + ")");
			    $('#big_slider a').attr("href", "page.php?url=" + urlArray[selectedNumber]);
			    $(this).css("background-color", "white");
	        	$('a', this).attr("href", "page.php?url=" + urlArray[selectedNumber]);// 'a' element inside "this".
			});


	    });



	</script>

</body>
</html>