<?php


error_reporting(E_ALL);
ini_set('display_errors', 1);

include 'connection.php';
include 'Comment.php';
include 'Article.php';

if ($_POST['action'] == 'comment-send')
{
	$commentnick = $_POST['commentnick'];
	$commenttext = $_POST['commenttext'];
	$article_id  = $_POST['article_id'];

	$myStatus = Comment::send($commentnick, $commenttext, $article_id);

	if ($myStatus)
	{
		$congrat_message = "great job";
		echo json_encode($congrat_message);
	}
}


if ($_POST['action'] == 'article-pull')
{
	$articles = Article::pullLatest();
	if ($articles)
	// if(Article::pull())
	{
		echo json_encode($articles);
	}
}



if ($_POST['action'] == 'comment-moderate')
{
	$decision  = $_POST['decision'];
	$comment_id = $_POST['comment_id'];

	$myStatus = Comment::moderate($decision, $comment_id);
	if($myStatus)
	{
		echo json_encode($myStatus);
	}
}



if($_POST['action'] == "article-add")
{

	$article_title = $_POST['article_title'];
	$article_text  = $_POST['article_text'];
	if (!empty($_FILES['upload_photo']))
	{
		$upload_photo = $_FILES['upload_photo'];
		echo json_encode(Article::add($article_title, $article_text, $upload_photo));
	}
	else
	{
		echo json_encode('Please put a file');
	}

}



?>